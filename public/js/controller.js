var app = angular.module('app', []);

app.controller('controller', controller);


function controller($scope, $http) {
    /** Variable declarada con valores por defecto para un test mas rapido **/
    $scope.data = {
        adults: 1,
        child: 1,
        destinationLat: 1,
        destinationLng: 1,
        infants: 1,
        lang: "EN",
        originLat: 0,
        originLng: 0,
        pickupDate_: new Date(),
        returnDate_: new Date(),
        pickupDate: "",
        returnDate: ""
    };
    $scope.response = null;
    $scope.error = false;

    $scope.submit = function (form) {

        $scope.data.pickupDate = getFormatDate($scope.data.pickupDate_);
        $scope.data.returnDate = getFormatDate($scope.data.returnDate_);

        $http.get('api', { params: $scope.data }).then(onSuccess, onError);
    }

    function onSuccess(data) {
        var result = data.data;
        if(result.status && result.status === 500) {
            onError(null, result.message)
            return;
        }
        $scope.error = null;
        $scope.response = data.data;
        console.log(data)
    }

    function onError(data, error) {
        $scope.error = data || error;
        $scope.response = null;
        console.log(data);
    }

    function getFormatDate(date) {
        return date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate() + " 00:00"
    }
}