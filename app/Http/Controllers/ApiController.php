<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ClientException;
class ApiController extends Controller
{

    /* ESTO PODRIA IR EN ENV */
    private $key = 'gwCqrVgVeStFA7LXdUdlyQstSKb1GGOM';
    private $api = 'http://mytransfersapitest-env.jqpvqnc6ft.eu-west-1.elasticbeanstalk.com:5382/';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $params = [
            'query' =>  [
                'adults' => $request->adults,
                'child' => $request->child,
                'destinationLat' => $request->destinationLat,
                'destinationLng' => $request->destinationLng,
                'infants' => $request->infants,
                'lang' => $request->lang,
                'originLat' => $request->originLat,
                'originLng' => $request->originLng,
                'pickupDate' => $request->pickupDate,
                'returnDate' => $request->returnDate
                ]
            ];

        try {
            $client = new Client();
            $res = $client->request('GET', $this->api . $this->key .'/availabilities', $params);
            return $res->json();
        } catch (RequestException  $e) {
            return $e->getResponse()->getBody(true);
        }

    }
}
