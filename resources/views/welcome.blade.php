<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" ng-app="app">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Transfers</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

    <!-- Styles -->
    <style>
        html,
        body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Nunito', sans-serif;
            font-weight: 200;
            height: 100vh;
            margin: 0;
        }

        .full-height {
            height: 100vh;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }

        .content {
            text-align: center;
        }

        .title {
            font-size: 84px;
        }

        .links>a {
            color: #636b6f;
            padding: 0 25px;
            font-size: 13px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }

        .m-b-md {
            margin-bottom: 30px;
        }

    </style>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css"
        integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap-theme.min.css"
        integrity="sha384-6pzBo3FDv/PJ8r2KRkGHifhEocL+1X2rVCTTkUfGk7/0pbek5mMa1upzvWbrUbOZ" crossorigin="anonymous">

    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.7.8/angular.min.js"></script>
    <script src="{{ asset('js/controller.js') }}"></script>
</head>

<body ng-controller="controller" ng-cloak>

    <div class="container">
        <div class="page-header">
            <h1>Transfers</h1>
        </div>
        <div class="row">
            <form ng-submit="submit(form)" novalidate name="form">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="adults">Adultos</label>
                                <input required type="number" class="form-control" ng-model="data.adults" id="adults"
                                    placeholder="Numero de adultos">
                            </div>

                            <div class="form-group">
                                <label for="child">Niños</label>
                                <input type="number" class="form-control" ng-model="data.child" id="child"
                                    placeholder="Numero de niños">
                            </div>

                            <div class="form-group">
                                <label for="destinationLat">Latitud de destino</label>
                                <input required type="number" class="form-control" ng-model="data.destinationLat"
                                    id="destinationLat" placeholder="Latitud de destino">
                            </div>

                            <div class="form-group">
                                <label for="destinationLng">Longitud de destino</label>
                                <input required type="number" class="form-control" ng-model="data.destinationLng"
                                    id="destinationLng" placeholder="Longitud de destino">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="infants">Infantes</label>
                                <input type="int" class="form-control" ng-model="data.infants" id="infants"
                                    placeholder="Numero de infantes">
                            </div>

                            <div class="form-group">
                                <label for="lang ">Idioma</label>
                                <select id="lang" placeholder="Idioma" ng-model="data.lang" class="form-control">
                                    <option value="EN">Ingles</option>
                                    <option value="ES">Español</option>

                                </select>
                            </div>

                            <div class="form-group">
                                <label for="originLat">Latitud de Origen</label>
                                <input required type="number" class="form-control" ng-model="data.originLat"
                                    id="originLat" placeholder="Latitud de origen">
                            </div>

                            <div class="form-group">
                                <label for="originLng">Longitud de Origen</label>
                                <input required type="number" class="form-control" ng-model="data.originLng"
                                    id="originLng" placeholder="Longitud de origen">
                            </div>

                            <div class="form-group">
                                <label for="pickupDate">Fecha pickup</label>
                                <input required type="date" class="form-control" ng-model="data.pickupDate_"
                                    id="pickupDate" placeholder="Fecha de pickup">
                            </div>

                            <div class="form-group">
                                <label for="returnDate">Fecha de regreso</label>
                                <input required type="date" class="form-control" ng-model="data.returnDate_"
                                    id="returnDate" placeholder="Fecha de regreso">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-2 col-md-offset-10 "> <button type="submit"
                                class="btn btn-success btn-block">Enviar</button>
                            <div>
                            </div>

                        </div>
            </form>
        </div>
        <hr>

        <div class="row">
            <div class="alert alert-success" ng-show="response" role="alert"> Resultados: </div>
            <div class="alert alert-warning" ng-show="error" role="alert"> No se encontraron resultados - <b>  @{{error}} </b></div>
        </div>
        <div class="row" ng-show="response">

            <div class="col-sm-12">
                <dl class="dl-horizontal">
                    <dt>Adultos:</dt>
                    <dd>@{{response.adults }}</dd>

                    <dt>Niños:</dt>
                    <dd>@{{response.childs }}</dd>

                    <dt>Latidud de Destino:</dt>
                    <dd>@{{response.destinationLat }}</dd>

                    <dt>Longitud de Destino:</dt>
                    <dd>@{{ response.destinationLng }}</dd>

                    <dt>Tipo:</dt>
                    <dd>@{{  response.dropoffType }}</dd>

                    <dt>Fecha:</dt>
                    <dd>@{{  response.fromDate | date }}</dd>

                    <dt>Infantes:</dt>
                    <dd>@{{ response.infants }}</dd>

                    <dt>Una ruta:</dt>
                    <dd>@{{ response.oneWay }}</dd>

                    <dt>Tipo de pickup:</dt>
                    <dd>@{{ response.pickupType }}</dd>

                    <dt>Sesion Id:</dt>
                    <dd>@{{response.sessionId }}</dd>

                    <dt>A Aeropuerto:</dt>
                    <dd>@{{response.toAirport }}</dd>

                    <dt>A Fecha:</dt>
                    <dd>@{{response.toDate | date}}</dd>
                </dl>
                <div class="col-sm-12" ng-repeat="transfers in response.transferPriceList">
                    <dl class="dl-horizontal">
                        <dt>Maximo de pasajeros:</dt>
                        <dd>@{{transfers.maxPassengers }}</dd>

                        <dt>Minimo de pasajeros:</dt>
                        <dd>@{{response.minPassengers }}</dd>

                        <dt>Precio:</dt>
                        <dd>@{{response.price }}</dd>

                        <dt>Suitcases:</dt>
                        <dd>@{{ response.suitcases }}</dd>

                        <dt>Transporte Id:</dt>
                        <dd>@{{  response.transportId }}</dd>

                        <dt>Nombre de transporte:</dt>
                        <dd>@{{  response.transportName }}</dd>
                    </dl>
                </div>
            </div>
        </div>
    </div>



    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"
        integrity="sha384-aJ21OjlMXNL5UyIl/XNwTMqvzeRMZH2w8c5cRVpzpU8Y5bApTppSuUkhZXN0VxHd" crossorigin="anonymous">
    </script>
</body>


</html>
